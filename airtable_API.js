var request = require('request');
require('dotenv').config();

//getRCEvent
function getRCEvent(event_id) {
    return new Promise(async function (resolve, reject) {
      var options = {
        'method': 'GET',
        'url': 'https://api.airtable.com/v0/'+process.env.BASE_ID+'/RC%20Events/'+event_id,
        'headers': {
          'Authorization': 'Bearer '+process.env.API_KEY,
        }
      };
  
      request(options, function (error, response) {
        console.log(response.statusCode)
        if (response.statusCode == 200) {
          resolve(JSON.parse(response.body)['fields'])
        } else {
          console.log(error)
          reject(error)
        }
      })
    })
  }

  //getRCContact
function getRCContact(contact_id) {
    return new Promise(async function (resolve, reject) {
        var options = {
            'method': 'GET',
            'url': 'https://api.airtable.com/v0/'+process.env.BASE_ID+'/RC%20Contacts/'+contact_id,
            'headers': {
              'Authorization': 'Bearer '+process.env.API_KEY,
            }
          };
  
      request(options, function (error, response) {
        if (response.statusCode == 200) {
          resolve(JSON.parse(response.body)['fields'])
        } else {
          console.log(error)
          reject(error)
        }
      })
    })
  }

//getRCAccount
function getRCAccount(client_id) {
    return new Promise(async function (resolve, reject) {
        var options = {
            'method': 'GET',
            'url': 'https://api.airtable.com/v0/'+process.env.BASE_ID+'/RC%20Accounts/'+client_id,
            'headers': {
              'Authorization': 'Bearer '+process.env.API_KEY,
            }
          };
          
  
      request(options, function (error, response) {
        if (response.statusCode == 200) {
          resolve(JSON.parse(response.body)['fields'])
        } else {
          console.log(error)
          reject(error)
        }
      })
    })
  }
  
  //getFacilitator
function getFacilitator(facilitator_id) {
    return new Promise(async function (resolve, reject) {
        var options = {
            'method': 'GET',
            'url': 'https://api.airtable.com/v0/'+process.env.BASE_ID+'/Facilitators/'+facilitator_id,
            'headers': {
              'Authorization': 'Bearer '+process.env.API_KEY
            }
          };
          
  
      request(options, function (error, response) {
        if (response.statusCode == 200) {
          resolve(JSON.parse(response.body)['fields'])
        } else {
          console.log(error)
          reject(error)
        }
      })
    })
  }

//updateDriveURL
function updateDriveURL(rec_id,drive_url,missing_fields) {
  return new Promise(async function (resolve, reject) {
    var options = {
      'method': 'PATCH',
      'url': 'https://api.airtable.com/v0/'+process.env.BASE_ID+'/RC%20Events',
      'headers': {
        'Authorization': 'Bearer '+process.env.API_KEY,
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        "records": [
          {
            "id": rec_id,
            "fields": {
              "drive_url": drive_url,
              "missing_fields":missing_fields
            }
          }
        ]
      })
    
    };
    request(options, function (error, response) {
      if (response.statusCode == 200) {
        resolve("Done")
      } else {
        console.log(error)
        reject(error)
      }
    })
  })
}


  

module.exports.getRCEvent=getRCEvent
module.exports.getRCAccount=getRCAccount
module.exports.getFacilitator=getFacilitator
module.exports.getRCContact=getRCContact
module.exports.updateDriveURL=updateDriveURL