var airtable_API = require('./airtable_API')


//RC_Event
async function fetch(rec_id) {
    var data = {};
    var missing="";
    return new Promise(async function (resolve, reject) {
        airtable_API.getRCEvent(rec_id).then((rc_event => {
            // console.log(String(rc_event['Sales Pipeline']).toLowerCase());
            console.log(rec_id);
            if (String(rc_event['Sales Pipeline']).toLowerCase() == 'confirmed') {
                if (rc_event['Start Date'] == "" || rc_event['Start Date'] == undefined) {
                    data['Start Date'] = "{start_date}"
                    missing=missing+" Start Date,"
                }
                else {
                    data['Start Date'] = rc_event['Start Date']
                }

                if (rc_event['End Date'] == "" || rc_event['End Date'] == undefined) {
                    data['End Date'] = "{end_date}"
                    missing=missing+" End Date,"
                }
                else {
                    data['End Date'] = rc_event['End Date']
                }

                if (data['Start Date']==data['End Date']) {
                    console.log("Matched");
                    data['End Date']=""
                }

                if (rc_event['Event'] == "" || rc_event['Event'] == undefined) {
                    data['Event'] = "{event}"
                    missing=missing+" Event,"
                }
                else {
                    data['Event'] = rc_event['Event']
                }

                if (rc_event['Time'] == "" || rc_event['Time'] == undefined) {
                    data['Time'] = "{time}"
                    missing=missing+" Time,"
                }
                else {
                    data['Time'] = rc_event['Time']
                }

                if (rc_event['Event Address'] == "" || rc_event['Event Address'] == undefined) {
                    data['Event Address'] = "{event_address}"
                    missing=missing+" Event Address,"
                }
                else {
                    data['Event Address'] = rc_event['Event Address']
                }

                if (rc_event['Client Fee'] == "" || rc_event['Client Fee'] == undefined) {
                    data['Event Fee'] = "{event_fee}"
                    missing=missing+" Event Fee,"
                }
                else {
                    data['Event Fee'] = rc_event['Client Fee']
                }

                if (rc_event['Approved Photo/Video'] == "" || rc_event['Approved Photo/Video'] == undefined) {
                    data['Approved Photo/Video'] = "None"
                    missing=missing+" Approved Photo/Video,"
                }
                else {
                    data['Approved Photo/Video'] = rc_event['Approved Photo/Video']
                }

                if (rc_event['Standardized Event Description'] == "" || rc_event['Standardized Event Description'] == undefined) {
                    data['Standardized Event Description'] = ""
                }
                else {
                    data['Standardized Event Description'] = rc_event['Standardized Event Description']
                }


                //RC_Account
                var client_id;
                if (rc_event['Client'] == "" || rc_event['Client'] == undefined) {
                    client_id = ""
                }
                else {
                    client_id = rc_event['Client'][0]
                }
                airtable_API.getRCAccount(client_id).then((rc_account) => {

                    if (rc_account['Name'] == "" || rc_account['Name'] == undefined) {
                        data['Client Name'] = "{client_name}"
                        missing=missing+" Client Name,"
                    }
                    else {
                        data['Client Name'] = rc_account['Name']
                    }


                    //Facilitator
                    var facilitator_id;
                    if (rc_event['Facilitator'] == "" || rc_event['Facilitator'] == undefined) {
                        facilitator_id = ""
                    }
                    else {
                        facilitator_id = rc_event['Facilitator'][0]
                    }
                    airtable_API.getFacilitator(facilitator_id).then((facilitator) => {
                        if (facilitator['Name'] == "" || facilitator['Name'] == undefined) {
                            data['Facilitator Name'] = "{facilitator_name}"
                            missing=missing+" Facilitator Name,"
                        }
                        else {
                            data['Facilitator Name'] = facilitator['Name']
                        }


                        //RC_Contact

                        var contact_id;
                        if (rc_event['Primary Contact'] == "" || rc_event['Primary Contact'] == undefined) {
                            contact_id = ""
                        }
                        else {
                            contact_id = rc_event['Primary Contact'][0]
                        }
                        airtable_API.getRCContact(contact_id).then((rc_contact) => {
                            if (rc_contact['Name'] == "" || rc_contact['Name'] == undefined) {
                                data['Primary Contact'] = "{primary_contact}"
                                missing=missing+" Primary Contact,"
                            }
                            else {
                                data['Primary Contact'] = rc_contact['Name']
                            }

                            if (rc_contact['Legal Business Address'] == "" || rc_contact['Legal Business Address'] == undefined) {
                                data['Legal Business Address'] = "{legal_business_address}"
                                missing=missing+" Legal Business Address,"
                            }
                            else {
                                data['Legal Business Address'] = rc_contact['Legal Business Address']
                            }

                            if (rc_contact['E-mail'] == "" || rc_contact['E-mail'] == undefined) {
                                data['Primary Contact E-mail'] = "{primary_contact_e_mail}"
                                missing=missing+" Primary Contact E-mail,"
                            }
                            else {
                                data['Primary Contact E-mail'] = rc_contact['E-mail']
                            }
                            data['missing']=missing
                            console.log(data);
                            resolve(data)
                        }).catch((err) => {
                            console.log("Error in fetching Contact details " + err);
                            reject({})
                        })


                    }).catch((err) => {
                        console.log("Error in fetching Facilitator details " + err);
                        reject({})
                    })



                }).catch((err) => {
                    console.log("Error in fetching Account details " + err);
                    reject({})
                })



            }
            else {
                console.log("Sales Pipeline is not Confirmed.");
                reject({})
            }



        })).catch((err) => {
            console.log("Error in fetching Event Details " + err);
            reject({})
        })
    })
}

module.exports.fetch = fetch
// fetch('recRQRUREerGDBJXM')