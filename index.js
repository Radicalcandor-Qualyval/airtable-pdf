const {createReport} = require("docx-templates");
const uploadToDrive = require("./uploadDrive.js");
const airtable_API = require("./airtable_API");
const fetch = require("./fetch_await")
const pdfConvert = require("./pdf_converter.js");
const fs = require("fs");
const path = require("path");

// template docs file
// let template = fs.readFileSync("./sow_template.docx");
let template = fs.readFileSync("sow_template.docx");

module.exports.handler = async function(event, context){
  // function to render template word file
  try {
    let recid = event.queryStringParameters.recid;
    if(!recid) throw new Error("No record id passed")
    let data = await fetch.fetch(recid);
    let data_transform = {};
    Object.keys(data).forEach((k) => {
      data_transform[k.toLowerCase().replace(/[\s|\/|-]/gi, "_")] = data[k];
    })
    data_transform.event_description = data_transform.standardized_event_description;
    data_transform.approved = data_transform.approved_photo_video == "None" ? false : true;
    data_transform.event_description = [
      "virtual keynote presentation",
      "virtual workshop",
      "in-person keynote presentation",
      "in-person workshop"
    ].indexOf(data_transform.event_description.toLowerCase());
    console.log(data_transform)
    
    // render the word template 
    let buff = await createReport({
      template, // template word file buffer
      cmdDelimiter: ['{', '}'], // wrapping characters for js code or data
      data: data_transform
    })

    // create a new word file with rendered data (buff)
    fs.writeFileSync(`/tmp/${data_transform.event}.docx`, buff);
    let fileName = data_transform.event;
    var date = new Date();
    var dateFormat=date.getDate() + "_" + (date.getMonth()+1) + "_" + date.getFullYear() + "_" + date.getHours() + "_" + date.getMinutes() + "_" + date.getSeconds();
    fs.writeFileSync(`/tmp/${fileName}_${dateFormat}.docx`, buff);

    // google drive functions
    let drive_upload = await uploadToDrive(fileName, dateFormat);
    await pdfConvert.pdfhandler(drive_upload.file_id, `${fileName}_${dateFormat}`);
    let pdf_drive_upload = await uploadToDrive(fileName, dateFormat, true)
    console.log(pdf_drive_upload);
    let drive_url = "https://drive.google.com/drive/folders/" + drive_upload.folder_id;
    var missing_fields=data_transform.missing
    // if (missing_fields!="") {
    //   missing_fields=missing_fields+" are missing."
    // }
    console.log(missing_fields);
    airtable_API.updateDriveURL(recid, drive_url, missing_fields);
    console.log(drive_url);
    return {
      success: true,
      drive_url,
      missing_fields
    }
  } catch( e ){
    console.log(e);
    let msg = !!e ? e.message : "Check AWS Logs"  
    return {
      success: false,
      msg
    }
  }
}