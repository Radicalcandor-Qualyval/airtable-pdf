"use strict";
const drive = require("@googleapis/drive");
const docs = require("@googleapis/docs");
const oauth = require("@googleapis/oauth2");
const axios = require("axios");
const fs = require("fs");
const path = require("path");

module.exports.pdfhandler = async (wordFileId, pdfFileName) => {
  // const oauth2client = new oauth.auth.OAuth2(
  //   process.env.CLIENT_ID,
  //   process.env.CLIENT_SECRET,
  //   process.env.REDIRECT_URL
  // );

  // oauth2client.setCredentials({
  //   refresh_token: process.env.REFRESH_TOKEN,
  // });

  // const Docs = docs.docs({ version: "v1", oauth2client });
  // const Drive = drive.drive({ version: "v3", oauth2client });
  const KEYFILEPATH = path.join(__dirname, 'credentials.json')
  const SCOPES = ['https://www.googleapis.com/auth/drive', 'https://www.googleapis.com/auth/docs']

  console.log()
  const auth = new drive.auth.GoogleAuth({
      keyFile: KEYFILEPATH,
      scopes: SCOPES,
  });

  const Docs = docs.docs({ version: "v1", auth });
  const Drive = drive.drive({ version: "v3", auth });

  // try {
  //   const response = await Drive.files.create({
  //     auth: oauth2client,
  //     requestBody: {
  //       name: `againtest.docx`,
  //       mimeType:
  //         "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  //     },
  //     media: {
  //       mimeType:
  //         "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
  //       body: fs.createReadStream(`${path.join(__dirname, "pdfs")}/test.docx`),
  //     },
  //   });
  // } catch (err) {
  //   console.log("Error while uploading doc file.");
  // }

  const fileId = wordFileId;

  //changing permission
  try {
    const res = await Drive.permissions.create({
      auth: auth,
      fileId: fileId,
      requestBody: {
        role: "reader",
        type: "anyone",
      },
    });
  } catch (err) {
    console.log("Error generated while changing permission");
    throw err
  }
  //generating pdf
  const pdflink = `https://docs.google.com/document/d/${fileId}/export?format=pdf`;
  try {
    await downloadPdf(pdflink);
    console.log("finished pdf")
  } catch (err) {
    console.log("Download error");
    throw err
  }
  async function downloadPdf(pdflink) {
    const writer = fs.createWriteStream(`/tmp/${pdfFileName}.pdf`);

    const res = await axios({
      url: pdflink,
      method: "GET",
      responseType: "stream",
    });

    res.data.pipe(writer);

    return new Promise((resolve, reject) => {
      writer.on("finish", resolve);
      writer.on("error", reject);
    });
  }

  //uploading pdf
  // const upload = await Drive.files.create({
  //   auth: oauth2client,
  //   requestBody: {
  //     name: `testupload02`,
  //     mimeType: "application/pdf",
  //   },
  //   media: {
  //     mimeType: "application/pdf",
  //     body: fs.createReadStream("/tmp/test.pdf"),
  //   },
  // });
  // //console.log(upload.data);
  // return {
  //   statusCode: 200,
  //   body: "success",
  // };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};
