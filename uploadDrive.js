const google = require('@googleapis/drive');
const fs = require('fs');
const path = require("path");
require('dotenv').config();

async function isFolderPresent(drive, parentFolder, folderName) {
    return new Promise(async (resolve, reject) => {

        //Check if the folder already exists
        var pageToken = null;
        drive.files.list({
            q: `mimeType = 'application/vnd.google-apps.folder' and name='${folderName}' and parents in '${parentFolder}'`,
            fields: 'nextPageToken, files(id, name)',
            spaces: 'drive',
            pageToken: pageToken
        }, function (err, res) {
            if (err) {
                // Handle error
                console.error(err);
                return reject(err);
            } else {
                // res.data.files.forEach(function (file) {
                //     console.log('Found file: ', file.name, file.id);
                // });
                pageToken = res.nextPageToken;
                if (res.data.files.length >= 1) {
                    return resolve({ status: true, folderId: res.data.files[0].id });
                } else {
                    return resolve({ status: false, folderId: null });
                }
            }
        });
    })
}

//Make folder
async function makeFolder(drive, parentFolder, folderName) {
    return new Promise(async (resolve, reject) => {
        var fileMetadata = {
            'name': folderName,
            'mimeType': 'application/vnd.google-apps.folder',
            parents: [parentFolder]
        };
        drive.files.create({
            resource: fileMetadata,
            fields: 'id'
        }, async function (err, file) {
            if (err) {
                console.error(err);
                return reject(err)
            } else {
                console.log('Folder Id: ', file.data.id);
                return resolve(file.data.id);
            }
        });
    })
}

//PDF And Word Upload
function uploadToDrive(fileName, dateFormat, pdf) {
    return new Promise(async (resolve, reject) => {
        const KEYFILEPATH = path.join(__dirname, 'credentials.json')
        const SCOPES = ['https://www.googleapis.com/auth/drive']

        const auth = new google.auth.GoogleAuth({
            keyFile: KEYFILEPATH,
            scopes: SCOPES,
        });

        const drive = google.drive({ version: "v3", auth });
        var folder_id;
        let obj = await isFolderPresent(drive, process.env.DRIVE_FOLDER_ID, fileName);
        if (obj.status == true) {
            console.log("Found");
            folder_id = obj.folderId;
        } else {
            console.log("NotFound");
            folder_id = await makeFolder(drive, process.env.DRIVE_FOLDER_ID, fileName);
        }
        console.log(folder_id);

        //PDF
        if(pdf){
            var fileMetadata = {
                'name': fileName + "_" + dateFormat + ".pdf",
                parents: [folder_id]
            };

            var media = {
                mimeType: '',
                body: fs.createReadStream(path.join("/tmp",fileName + "_" + dateFormat + ".pdf"))
            };

            drive.files.create({
                resource: fileMetadata,
                media: media,
                fields: 'id'
            }, function (err, file) {
                if (err) {
                    // Handle error
                    console.log((err));
                    return reject(err);
                } else {
                    //console.log(file);
                    console.log('File Id: ', file.data.id);
                    return resolve(folder_id)

                }
            });
        }

        //Docx
        if(!pdf){
            var fileMetadata = {
                'name': fileName + "_" + dateFormat + ".docx",
                parents: [folder_id]
            };

            var media = {
                mimeType: '',
                body: fs.createReadStream(path.join("/tmp", fileName + "_" + dateFormat + ".docx"))
            };

            drive.files.create({
                resource: fileMetadata,
                media: media,
                fields: 'id'
            }, function (err, file) {
                if (err) {
                    // Handle error
                    console.log((err));
                    return reject(err);
                } else {
                    //console.log(file);
                    console.log('File Id: ', file.data.id);
                    return resolve({folder_id, file_id: file.data.id})

                }
            });
        }
    })
}



module.exports = uploadToDrive;